# Belajar Git #

1. Membuat SSH Keypair (pasangan public key dan private key)


    a. Buka `Git Bash`

    b. Jalankan perintah berikut di dalam git bash

        ssh-keygen
    
    c. Buka file `C:\Users\endy\.ssh\id_rsa.pub` dengan VSCode atau Notepad

    d. Copy dan paste di Gitlab.com > Preferences > SSH Keys

    [![Gitlab Preferences](img/gitlab-preferences.png)](img/gitlab-preferences.png)

    [![SSH Keys](img/gitlab-ssh-key.png)](img/gitlab-ssh-key.png)
    
    e. Clone repository yang sudah ada

        git clone <URL>
        
      Contohnya

        git clone git@gitlab.com:training-devops-2022-01/belajar-devops.git



## Referensi ##

* [Git Workflow](https://docs.google.com/presentation/d/1kktaQTBgAkZPY-7R3ZrB5rR3JhqIjsFUnZyL0joTF_c/edit#slide=id.p)
